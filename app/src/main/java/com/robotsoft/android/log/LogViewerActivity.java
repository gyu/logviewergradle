package com.robotsoft.android.log;

import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

public class LogViewerActivity extends TabActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Class clz=Util.isActive(this)?LogMain.class:SettingsActivity.class;
		try {
			Intent i = new Intent(this, clz);
			startActivity(i);
			finish();
		} catch (Throwable e) {
			Log.e("LogView", "Starting Error", e);
		}
	}

}