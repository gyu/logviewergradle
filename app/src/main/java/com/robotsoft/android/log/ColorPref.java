package com.robotsoft.android.log;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class ColorPref extends PreferenceActivity {
	public static String Debug = "debug_color";
	public static String Info = "info_color";
	public static String Error = "error_color";
	public static String FullTime = "full_time";
	public static String ShowPid = "show_pid";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.color);
	}

}
