package com.robotsoft.android.log;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class EmailPref extends PreferenceActivity {
	public static String DateFormat = "date_format";
	public static String Subject = "subject_line";
	public static String DialogTitle = "dialog_title";
	public static String SendTo = "send_to";
	public static String SendFormat = "send_format";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.email);
	}

}
