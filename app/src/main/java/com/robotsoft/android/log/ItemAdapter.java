package com.robotsoft.android.log;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;


public class ItemAdapter extends ArrayAdapter<Tag> {
	private List<Tag> marked;
	private TagActivity parent;

	public ItemAdapter(TagActivity context, List<Tag> list) {
		super(context, R.layout.tag_item, list);
		parent=context;
		marked = new ArrayList<Tag>();
		for (Tag s : list) {
			if (s!=null&&s.isActive())
				marked.add(s);
		}
	}

	public void checkSelected() {
		int n = getCount();
		boolean update = false;
		for (int i = 0; i < n; i++) {
			Tag s = getItem(i);
			if(s!=null) {
			if (s.isSelected()) {
				update = true;
				break;
			} else if (s.isActive() && !marked.contains(s)) {
				update = true;
				break;
			} else if (!s.isActive() && marked.contains(s)) {
				update = true;
				break;
			}
			}
		}
		parent.showButtons(update);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
		if (position == 0) {
			return inflater.inflate(R.layout.create_new_tag, parent,
					false);
		}
		Tag s = getItem(position);
		TagItem rowView = (TagItem) inflater.inflate(
				R.layout.tag_item, null, true);
		rowView.bindView(this, s);
		return rowView;
	}

}
