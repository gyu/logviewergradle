package com.robotsoft.android.log;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

public class FilterActivity extends Activity {
	public static final String LastTime="last_time";
	public static final String LastLine="last_line";
	public static final String TextContained="text_contained";
	public static final String Level="log_level";
	
	private CheckBox timeCheck;
	private CheckBox lineCheck;
	private CheckBox textCheck;
	private EditText lastTime;
	private EditText lastLine;
	private EditText text;
	private Spinner level;
	
	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.filter);
			lineCheck=(CheckBox)findViewById(R.id.last_lines);
			timeCheck=(CheckBox)findViewById(R.id.last_time);
			textCheck=(CheckBox)findViewById(R.id.contain_chek);
			lastLine=(EditText)findViewById(R.id.last_line_number);
			lastTime=(EditText)findViewById(R.id.last_hour);
			text=(EditText)findViewById(R.id.inxlude_text);
			level=(Spinner)findViewById(R.id.level);
			ArrayAdapter adpt = ArrayAdapter.createFromResource(this,
					R.array.levels,
					android.R.layout.simple_spinner_item);
			adpt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			level.setAdapter(adpt);
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
			String timeVal=prefs.getString(LastTime, null);
			String lineVal=prefs.getString(LastLine, "100");
			String textVal=prefs.getString(TextContained, null);
			String levelVal=prefs.getString(Level, "Debug");
			if(timeVal!=null) {
				timeCheck.setChecked(true);
				lastTime.setText(timeVal);
			}
			if(!lineVal.equals("")) {
				lineCheck.setChecked(true);
				lastLine.setText(lineVal);
			}
			if(textVal!=null) {
				textCheck.setChecked(true);
				text.setText(textVal);
			}
			String[] a=getResources().getStringArray(R.array.levels);
			for(int i=0; i<a.length; i++) {
				if(a[i].equalsIgnoreCase(levelVal)) {
					level.setSelection(i);
					break;
				}
			}
			
		}catch(Throwable e) {
			Log.e("LogView","Error in filter",e);
		}
	}
	
	public void popultate() {
		SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(this).edit();
		String timeVal=timeCheck.isChecked()? lastTime.getText().toString():null;
		String lineVal=lineCheck.isChecked()? lastLine.getText().toString():"";
		String textVal=textCheck.isChecked()? text.getText().toString():null;
		if(timeVal!=null&&timeVal.length()==0)timeVal=null;
		if(lineVal!=null&&lineVal.length()==0)lineVal=null;
		if(textVal!=null&&textVal.length()==0)textVal=null;
		prefs.putString(Level, level.getSelectedItem().toString());
		prefs.putString(LastTime, timeVal);
		prefs.putString(LastLine, lineVal);
		prefs.putString(TextContained, textVal);
		prefs.commit();
	}
	
	public void onBackPressed() {
		if(getParent()!=null) {
			getParent().onBackPressed();
		}
		else {
			super.onBackPressed();
		}
	}

}
