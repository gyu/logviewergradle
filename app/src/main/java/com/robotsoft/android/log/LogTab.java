package com.robotsoft.android.log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.LinearLayout;



public class LogTab extends Activity {
	public final static String LINE_SEPARATOR = System.getProperty("line.separator");
	private WebView view;
	private String logTag;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab);
        view=(WebView)findViewById(R.id.tab);
        Log.d("LogView","Add view: "+view );
        logTag=getIntent().getStringExtra("tag");
        refresh(true);
    }

	public void refresh(boolean showDialog) {
		FetchTask t = new FetchTask(showDialog);
		t.execute(logTag);
	}
	
	class FetchTask extends AsyncTask<String, Void, String> {
		private ProgressDialog dialog;
		private boolean showDialog;
		
		FetchTask(boolean showDialog) {
			this.showDialog=showDialog;
		}
		
		@Override
		protected void onPreExecute() {
			if(showDialog) {
			dialog=new ProgressDialog(LogTab.this);
			dialog.setMessage(getString(R.string.fetch_message));
		    dialog.show();
			}
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			Config cfg=Util.getConfig(LogTab.this);
	        StringBuilder log = new StringBuilder("<html><body>");
	        BufferedReader bufferedReader=null;
	        try{
	        	String commandline;
	        	if(params[0]==null) {
	        		commandline="logcat -d -v threadtime *:"+Util.getLevel(LogTab.this);
	        	}
	        	else {
	        		commandline="logcat -d -v threadtime -s "+params[0]+":"+Util.getLevel(LogTab.this);
	        	}
	        	Log.d("LogView",commandline);
	            Process process = Runtime.getRuntime().exec(commandline);
					bufferedReader = new BufferedReader(new InputStreamReader(
							process.getInputStream()));
					String line;
					Filter filter=Util.getFilter(LogTab.this);
					int lines=filter.getLineCount();
					List<String> list=null;
					if(lines!=-1) {
						list=new ArrayList<String>();
					}
					int prefixLen=Util.dataFormatLen(LogTab.this);
					while ((line = bufferedReader.readLine()) != null) {
						Record rec=Util.toRecord(line, logTag, filter, prefixLen);
						if(rec!=null) {
							line=Util.toString(rec, cfg);
							if(list==null) {
								log.append(line);
							}
							else {
								if(list.size()>=filter.getLineCount()){
									list.remove(0);
								}
								list.add(line);
							}
						}
					}
					if(list!=null) {
						for(int i=0; i<list.size(); i++) {
							log.append(list.get(i));
						}
					}
					process.waitFor();
	        } 
	        catch (IOException e){
	            Log.e("LogView", "CollectLogTask doInBackground failed", e);//$NON-NLS-1$
	        }
	        catch (Throwable e){
	            Log.e("LogView", "CollectLogTask failed", e);//$NON-NLS-1$
	        }
	        finally {
	        	if(bufferedReader!=null) {
	        		try {
						bufferedReader.close();
					} catch (IOException e) {
					}
	        	}
	        }
	        return log.append("</body></html>").toString();
		}

		@Override
		protected void onPostExecute(String result) {
			//Log.d("LogView",result);
			if(dialog!=null) {
				dialog.dismiss();
				dialog=null;
			}
			view.loadData(result, "text/html", "utf-8");
		}
	}

	public void clear() {
		view.loadData("<html><body></body></html>", "text/html", "utf-8");
	}

}
