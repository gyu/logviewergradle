package com.robotsoft.android.log;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TagItem extends RelativeLayout {
	private final static float CHECKMARK_PAD = 10.0F;
	private ItemAdapter adapter;
	private boolean mCachedViewPositions;
	private int mCheckRight;
	private boolean mDownEvent;
	private int mStarLeft;
	private Tag tag;
	

	public TagItem(Context context) {
		super(context);
	}

	public TagItem(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public TagItem(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void bindView(ItemAdapter adapter, Tag tag) {
		this.adapter = adapter;
		this.tag = tag;
		try {
			ImageView sel = (ImageView) findViewById(R.id.selected);
			sel.setImageDrawable(getResources().getDrawable(
					tag.isSelected() ? R.drawable.btn_on
							: R.drawable.btn_off));
			ImageView fav = (ImageView) findViewById(R.id.favorite);
			fav.setImageDrawable(getResources().getDrawable(
					tag.isActive() ? android.R.drawable.star_on
							: android.R.drawable.star_off));
			TextView claz = (TextView) findViewById(R.id.claz);
			claz.setText(tag.getValue() == null ? "" : tag.getValue());
			TextView name = (TextView) findViewById(R.id.name);
			name.setText(tag.getName());
			mCachedViewPositions = false;
		} catch (Throwable e) {
		}
	}

	public Tag getTag() {
		return tag;
	}

	/**
	 * Overriding this method allows us to "catch" clicks in the checkbox or
	 * star and process them accordingly.
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		boolean handled = false;
		int touchX = (int) event.getX();
		if (!mCachedViewPositions) {
			float paddingScale = getContext().getResources()
					.getDisplayMetrics().density;
			int checkPadding = (int) ((CHECKMARK_PAD * paddingScale) + 0.5);
			int starPadding = (int) ((CHECKMARK_PAD * paddingScale) + 0.5);
			mCheckRight = findViewById(R.id.selected).getRight() + checkPadding;
			mStarLeft = findViewById(R.id.favorite).getLeft() - starPadding;
			mCachedViewPositions = true;
		}
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			mDownEvent = true;
			if (touchX < mCheckRight || touchX > mStarLeft) {
				handled = true;
			}
			break;

		case MotionEvent.ACTION_CANCEL:
			mDownEvent = false;
			break;

		case MotionEvent.ACTION_UP:
			if (mDownEvent) {
				try {
					if (touchX < mCheckRight) {
						tag.setSelected(!tag.isSelected());
						ImageView sel = (ImageView) findViewById(R.id.selected);
						sel.setImageDrawable(getResources()
								.getDrawable(
										tag.isSelected() ? R.drawable.btn_on
												: R.drawable.btn_off));
						adapter.checkSelected();
						handled = true;
					} else if (touchX > mStarLeft) {
						tag.setActive(!tag.isActive());
						ImageView fav = (ImageView) findViewById(R.id.favorite);
						fav.setImageDrawable(getResources().getDrawable(
								tag.isActive() ? android.R.drawable.star_on
										: android.R.drawable.star_off));
						adapter.checkSelected();
						handled = true;
					}
				} catch (Throwable e) {
				}
			}
			break;
		}
		if (handled) {
			postInvalidate();
		} else {
			handled = super.onTouchEvent(event);
		}
		return handled;
	}

}
