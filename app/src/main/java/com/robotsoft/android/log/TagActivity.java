package com.robotsoft.android.log;

import java.util.ArrayList;

import java.util.List;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class TagActivity extends ListActivity implements OnItemClickListener {
	public static final String TagClass="tag_class";
	public static final String TagName="tag_name";
	public static final String TagActive="tag_active";
	public static final String ShowAll="show_all";
	private ItemAdapter adapter;
	private View buttonPanel;
	private int itemSelected=-1;
	private List<Tag> list;
	private CheckBox  checkAll;
	

	public void showButtons(boolean show) {
		if (show && buttonPanel.getVisibility() != View.VISIBLE) {
			buttonPanel.setVisibility(View.VISIBLE);
			buttonPanel.startAnimation(AnimationUtils.loadAnimation(this,
					R.anim.footer_appear));
		} else if (!show && buttonPanel.getVisibility() != View.GONE) {
			buttonPanel.setVisibility(View.GONE);
			buttonPanel.startAnimation(AnimationUtils.loadAnimation(this,
					R.anim.footer_disappear));
		}
	}

	private void add(String log, String name) {
		list.add(new Tag(name,log,true));
		update();
		adapter = new ItemAdapter(this, list);
		setListAdapter(adapter);
		
	}
	
	private void createNewTag() {
		try {
		LayoutInflater inflater = getLayoutInflater();
		View d = inflater.inflate(R.layout.dialog, null);
        final EditText f=(EditText)d.findViewById(R.id.filer_name);
        final EditText l=(EditText)d.findViewById(R.id.log_tag);
        
        AlertDialog.Builder builder =
            new AlertDialog.Builder(this);
        builder.setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle(R.string.new_filter)
        .setMessage(getString(R.string.create_new_tag))
        .setCancelable(true)
        .setPositiveButton(
                getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    	String log=l.getText().toString().trim();
                    	if(log.length()==0) {
                    		Toast.makeText(TagActivity.this, R.string.no_tag, Toast.LENGTH_LONG).show();
                    	}
                    	else {
                    		String name=f.getText().toString().trim();
                    		if(name.length()==0) {
                    			name=log;
                    		}
                    		add(log, name);
                    	}
                    }
                })
        .setNegativeButton(getString(R.string.cancel),null);
        builder.setView(d).show();
		}catch(Throwable e) {
			Log.e("LogView","Dialog Error",e);
		}
		
		
	}
	
	@Override
	protected void onListItemClick(ListView l, View view, int position, long id) {
		doClick(view);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		doClick(view);
	}
	
	private void doClick(View view) {
		if (!(view instanceof TagItem)) {
			createNewTag();
		}
	}
	
	
	@SuppressWarnings({ "UnusedDeclaration" })
	public void delete(View v) {
		doDelete();
	}

	@SuppressWarnings({ "UnusedDeclaration" })
	public void choose(View v) {
		doChoose();
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if(itemSelected!=-1) {
			int id=itemSelected;
			itemSelected=-1;
		}
		return true;
	}
	
	private void doDelete() {
		boolean p=false;
		for (int i = adapter.getCount() - 1; i >= 0; i--) {
			Object o = adapter.getItem(i);
			if(o!=null&&(o instanceof Tag)) {
				Tag t=(Tag)o;
				if(t.isSelected()) {
					adapter.remove(t);
					list.remove(t);
					p=true;
				}
			}
		}
		if(p) update();
	}
	
	private void doChoose() {
		List<String> marked = new ArrayList<String>();
		for (int i = 0, n = adapter.getCount(); i < n; i++) {
			Object o = adapter.getItem(i);
			if(o!=null&&(o instanceof Tag)) {
				Tag t=(Tag)o;
				if(t.isActive()) {
					marked.add(((Tag)o).getValue());
				}
			}
		}
		boolean p=false;
		List<Tag> list=Util.getTags(this);
		for (Tag s : list) {
			if (s!=null && s.isActive() != marked.contains(s.getValue())) {
				s.setActive(!s.isActive());
				p=true;
			}
		}
		if(p) update();
	}
	
	private List<Tag> getTags() {
		if(list==null) {
			list=new ArrayList<Tag>();
			list.add(null);
			list.addAll(Util.getTags(this));
		}
		return list;
	}
	
	private void update() {
		Util.update(list, this);
	}
	
	protected void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.tag);
			checkAll=(CheckBox)findViewById(R.id.check_all);
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
			boolean showAll = prefs.getBoolean(ShowAll, true);
			checkAll.setChecked(showAll);
			ListView view = getListView();
			view.setOnItemClickListener(this);
			view.setItemsCanFocus(false);
			adapter = new ItemAdapter(this, getTags());
			setListAdapter(adapter);
			buttonPanel = (View) this.findViewById(R.id.buttons);
		} catch (Throwable e) {
			Log.e("LogView","Error in tag tab",e);
		}
	}
	
	public void checkAll(View view) {
		boolean showAll =checkAll.isChecked();
		SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(this).edit();
		prefs.putBoolean(ShowAll, showAll);
		prefs.commit();
	}

	public void onBackPressed() {
		if(getParent()!=null) {
			getParent().onBackPressed();
		}
		else {
			super.onBackPressed();
		}
	}

}
