package com.robotsoft.android.log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class Util {
	private static String LB="<br>"+System.getProperty("line.separator");
	private static String ClrFormat="<font color=\"%s\">";
	
	public static boolean isActive(Context ctxt) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
		if( prefs.getBoolean(TagActivity.ShowAll, true)) {
			return true;
		}
		List<Tag> l=getTags(ctxt);
		for(Tag t:l) {
			if(t.isActive()) {
				return true;
			}
		}
		return false;
	}
	
	public static int dataFormatLen(Context ctxt) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
		String fmt=prefs.getString(EmailPref.DateFormat, "MM-dd HH:mm:ss.SSS");
		return fmt.length();
	}
	
	public static SimpleDateFormat dataFormat(Context ctxt) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
		String fmt=prefs.getString(EmailPref.DateFormat, "MM-dd HH:mm:ss.SSS");
		return new SimpleDateFormat(fmt);
	}
	
	public static Filter getFilter(Context ctxt) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
		String timeVal=prefs.getString(FilterActivity.LastTime, null);
		String lineVal=prefs.getString(FilterActivity.LastLine, "100");
		String textVal=prefs.getString(FilterActivity.TextContained, null);
		if(timeVal!=null) {
			Calendar cal=Calendar.getInstance();
			cal.add(Calendar.HOUR, -Integer.parseInt(timeVal));
			SimpleDateFormat df=dataFormat(ctxt);
			timeVal=df.format(cal.getTime());
		}
		return new Filter(lineVal.equals("")?-1:Integer.parseInt(lineVal), timeVal, textVal);
	}
	
	public static List<Tag> getTags(Context ctxt) {
		List<Tag> list=new ArrayList<Tag>();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
		String z = prefs.getString(TagActivity.TagClass, null);
		if(z!=null&&z.length()>0) {
			String[] vals=z.split("\\|");
			z = prefs.getString(TagActivity.TagName, null);
			String[] nams=z.split("\\|");
			z = prefs.getString(TagActivity.TagActive, null);
			String[] flags=z.split("\\|");
			for(int i=0; i<vals.length; i++) {
				list.add(new Tag(nams[i],vals[i],Boolean.parseBoolean(flags[i])));
			}
		}
		return list;
	}
	
	public static char getLevel(Context ctxt) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
		String z = prefs.getString(FilterActivity.Level,"Debug");
		return z.charAt(0);
	}

	public static String getFormat(Context ctxt) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
		return prefs.getString(EmailPref.SendFormat,"threadtime");
	}
	
	public static void update(List<Tag> list, Context ctxt) {
		SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(ctxt).edit();
		StringBuffer b1=new StringBuffer(); 
		StringBuffer b2=new StringBuffer();
		StringBuffer b3=new StringBuffer();
		for(int i=0,n=list.size(),j=0; i<n; i++) {
			Tag t=list.get(i);
			if(t!=null) {
				if(j>0) {
					b1.append('|');
					b2.append('|');
					b3.append('|');
				}
				b1.append(t.getValue());
				b2.append(t.getName());
				b3.append(String.valueOf(t.isActive()));
				j++;
			}
		}
		prefs.putString(TagActivity.TagClass, b1.toString());
		prefs.putString(TagActivity.TagName, b2.toString());
		prefs.putString(TagActivity.TagActive, b3.toString());
		prefs.commit();
	}
	
	public static boolean accept(String line, Filter filter, int len) {
		line=line.trim();
		if(line.length()<len) return false;
		String time=line.substring(0,len);
		if(filter.getTime()!=null&&time.compareTo(filter.getTime())<0) {
			return false;
		}
		line=line.substring(len).trim();
		int l=line.indexOf(":");
		String content=line.substring(l+1).trim();
		if(filter.getPattern()!=null&&content.indexOf(filter.getPattern())<0) {
			return false;
		}
		return true;
	}
	
	public static Record toRecord(String line, String tag, Filter filter, int len) {
		if(tag==null) {
		return dummyRecord(line,filter, len);	
		}
		return tagRecord(line,tag, filter, len);
	}
	
	private static Record tagRecord(String line, String tag, Filter filter, int len) {
		line=line.trim();
		if(line.length()<len) return null;
		String time=line.substring(0,len);
		if(filter.getTime()!=null&&time.compareTo(filter.getTime())<0) {
			return null;
		}
		line=line.substring(len).trim();
		int k=line.indexOf(tag);
		if(k<0) return null;
		int l=line.indexOf(":",k+tag.length());
		String content=line.substring(l+1).trim();
		if(filter.getPattern()!=null&&content.indexOf(filter.getPattern())<0) {
			return null;
		}
		line=line.substring(0,k).trim();
		l=line.length();
		int level=0;
		String pid;
		if(l<2||line.charAt(l-2)!=' ') {
			level=0;
			pid="";
		}
		else {
			switch(line.charAt(l-1)) {
			case 'I':case 'i':
				level=1; break;
			case 'E':case 'e':case 'W':case 'w':case 'F':case 'f':
				level=2; break;
			default:level=0; 
			}
			pid=line.substring(0,line.indexOf(' '));
			
		}
		return new Record( time, pid, content, level);
	}

	
	public static Record dummyRecord(String line,Filter filter, int len) {
		line=line.trim();
		if(line.length()<len) return null;
		String time=line.substring(0,len);
		if(filter.getTime()!=null&&time.compareTo(filter.getTime())<0) {
			return null;
		}
		char[] chars={'V','D','I','E','W','F'};
		int level=-1;
		for(char c:chars) {
			String a=" "+c+" ";
			if(line.indexOf(a)>=0) {
				level=c=='V'||c=='D'?0:c=='I'?1:2;
				break;
			}
		}
		if(level==-1) {
			level=0;
			for(char c:chars) {
				String a=" "+(char)(c-'A'+'a')+" ";
				if(line.indexOf(a)>=0) {
					level=c=='V'||c=='D'?0:c=='I'?1:2;
					break;
				}
			}	
		}
		return new Record( time, line.substring(len).trim(), level);
	}

	
	public static String toString(Record rec, Config cfg) {
		if(rec==null) return null;
		StringBuffer buf=new StringBuffer(String.format(ClrFormat, cfg.getColors()[rec.getLevel()]));
		String time=rec.getTime();
		if(!cfg.isShowFulltime()) {
			int k=time.lastIndexOf('.');
			if(k>0) time=time.substring(0,k);
		}
		buf.append("<i>").append(time).append("</i> ");
		if(rec.getPid()!=null&&cfg.isShowPid()){
			buf.append("&lt;").append(rec.getPid()).append("&gt; ");
		}
		buf.append(rec.getContent());
		buf.append(LB);
		return buf.toString();
	}
	
	public static Config getConfig(Context ctxt) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
		String[] colors=new String[]{prefs.getString(ColorPref.Debug, "#000000"),
				prefs.getString(ColorPref.Info, "#00ff00"),prefs.getString(ColorPref.Error, "#ff0000")};
		boolean showPid=prefs.getBoolean(ColorPref.ShowPid, true);
		boolean showFulltime=prefs.getBoolean(ColorPref.FullTime, false);
		return new Config(colors,showPid,showFulltime);
		
	}

}
