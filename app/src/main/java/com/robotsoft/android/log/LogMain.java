package com.robotsoft.android.log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TabHost;
import android.widget.Toast;

public class LogMain extends TabActivity {
	public final static String LINE_SEPARATOR = System
			.getProperty("line.separator");
	private String[] claz;
	private boolean showAll;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
			showAll= prefs.getBoolean(TagActivity.ShowAll, true);
				List<Tag> lst = Util.getTags(this);
				int n = lst.size();
				int m=0;
				for (int i = 0; i < n; i++) {
					if(lst.get(i).isActive() )m++;
				}
				claz = new String[m];
				TabHost host = getTabHost();
				if(showAll) {
					Intent intent = new Intent(this, LogTab.class);
					TabHost.TabSpec spec = host
					.newTabSpec(String.valueOf(0))
					.setIndicator(getString(R.string.all),getResources().getDrawable(R.drawable.tab_left)).setContent(intent);
					host.addTab(spec);
				}
				for (int i = 0, j = showAll?1:0,k=0; i < n; i++) {
					Tag t = lst.get(i);
					if (t.isActive()) {
						claz[k] = t.getValue();
						Intent intent = new Intent(this, LogTab.class);
						intent.putExtra("tag", claz[k]);
						int iconId=(j%2==0)?R.drawable.tab_left:R.drawable.tab_right;
						TabHost.TabSpec spec = host
								.newTabSpec(String.valueOf(j))
								.setIndicator(t.getName(),getResources().getDrawable(iconId)).setContent(intent);
						host.addTab(spec);
						j++;
						k++;
					}
				}
				
		} catch (Throwable e) {
			Log.e("LogView", "Error in main tab", e);
		}
	}

	private LogTab findTab(int index) {
		Activity  tab=getLocalActivityManager().getActivity(
				String.valueOf(index));
		return (LogTab)tab;
	}
	
	

	private void refresh() {
		TabHost host = getTabHost();
		int k=host.getCurrentTab();
		int n=claz==null?0:claz.length;
		if(showAll)n++;
		for (int j = 0; j < n; j++) {
			LogTab t=findTab(j);
			if(t!=null) t.refresh(j==k);
		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.main_option, menu);
		return true;
	}

	private void startConfig() {
		Intent j = new Intent(this, SettingsActivity.class);
		startActivity(j);
		this.finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		try {
			switch (item.getItemId()) {
			case R.id.preference:
				Intent i = new Intent(this, PrefManager.class);
				startActivityForResult(i, 0);
				return true;
			case R.id.send:
				new SendTask().execute(claz);
				return true;
			case R.id.delete:
				new CleanTask().execute();
				return true;
			case R.id.refresh:
				refresh();
				return true;
			case R.id.config:
				startConfig();
				return true;
			}
		} catch (Throwable e) {
			Log.e("LogView", "Menu Error", e);
		}
		return super.onOptionsItemSelected(item);
	}

	private class SendTask extends AsyncTask<String[], Void, String> {
		private ProgressDialog dialog;
		
		@Override
		protected void onPreExecute() {
			dialog=new ProgressDialog(LogMain.this);
			dialog.setMessage(getString(R.string.fetch_message));
		    dialog.show();
			super.onPreExecute();
		}
		
		@Override
		protected String doInBackground(String[]... params) {
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LogMain.this);
			boolean showAll= prefs.getBoolean(TagActivity.ShowAll, true);
			StringBuilder log = new StringBuilder();
			BufferedReader bufferedReader = null;
			try {
				String[] tags = params[0];
				char d=Util.getLevel(LogMain.this);
				StringBuffer bf = new StringBuffer("-d -v ");
				bf.append(Util.getFormat(LogMain.this));
				if(showAll) {
					bf.append(" *:").append(d);
				}
				else {
					bf.append(" -s");
					for (int i = 0; i < tags.length; i++) {
						bf.append(" ").append(tags[i]).append(":").append(d);
					}
				}
				Log.d("LogView","Process logcat "+bf.toString());
				Process process = Runtime.getRuntime().exec(
						"logcat " + bf.toString());
				bufferedReader = new BufferedReader(new InputStreamReader(
						process.getInputStream()));
				Filter filter=Util.getFilter(LogMain.this);
				int lines=filter.getLineCount();
				List<String> list=null;
				if(lines!=-1) {
					list=new ArrayList<String>();
				}
				int prefixLen=Util.dataFormatLen(LogMain.this);
				String line;
				while ((line = bufferedReader.readLine()) != null) {
					Log.d("LogView",line);
					if (Util.accept(line, filter, prefixLen)) {
						if (list == null) {
							log.append(line).append(LINE_SEPARATOR);
						} else {
							if (list.size() >= filter.getLineCount()) {
								list.remove(0);
							}
							list.add(line);
						}
					}
				}
				if(list!=null) {
					for(int i=0; i<list.size(); i++) {
						log.append(list.get(i)).append(LINE_SEPARATOR);
					}
				}
				process.waitFor();
			} catch (IOException e) {
				Log.e("LogView", "CollectLogTask doInBackground failed", e);//$NON-NLS-1$
			} catch (Throwable e) {
				Log.e("LogView", "CollectLogTask failed", e);//$NON-NLS-1$
			} finally {
				if (bufferedReader != null) {
					try {
						bufferedReader.close();
					} catch (IOException e) {
					}
				}
			}
			return log.toString();
		}

		@Override
		protected void onPostExecute(String result) {
			dialog.dismiss();
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(LogMain.this);
			String s = prefs.getString(EmailPref.SendTo, null);
			Intent emailIntent = new Intent(Intent.ACTION_SEND);
			emailIntent.setType("plain/text");
			if (s != null) {
				String[] ss = s.split("\\,");
				emailIntent.putExtra(Intent.EXTRA_EMAIL, ss);
			}
			s = prefs.getString(EmailPref.Subject, "Logging");
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, s);
			emailIntent.putExtra(Intent.EXTRA_TEXT, result);
			s = prefs.getString(EmailPref.DialogTitle, "Email logging");
			startActivity(Intent.createChooser(emailIntent, s));
		}
	}
	
	private class CleanTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			try {
				Process process = Runtime.getRuntime().exec("logcat -d -c");
				process.waitFor();
			} catch (IOException e) {
				Log.e("LogView", "CollectLogTask doInBackground failed", e);//$NON-NLS-1$
			} catch (Throwable e) {
				Log.e("LogView", "CollectLogTask failed", e);//$NON-NLS-1$
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void v) {
			int n=claz==null?0:claz.length;
			if(showAll)n++;
			for (int j = 0; j < n; j++) {
				LogTab t=findTab(j);
				if(t!=null) t.clear();
			}
		}
	}

}