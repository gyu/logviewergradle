package com.robotsoft.android.log;

public class Config {
	private String[] colors;
	private boolean showPid;
	private boolean showFulltime;
	public Config(String[] colors, boolean showPid, boolean showFulltime) {
		super();
		this.colors = colors;
		this.showPid = showPid;
		this.showFulltime = showFulltime;
	}
	
	public String[] getColors() {
		return colors;
	}
	
	public boolean isShowPid() {
		return showPid;
	}
	public boolean isShowFulltime() {
		return showFulltime;
	}
	
}
