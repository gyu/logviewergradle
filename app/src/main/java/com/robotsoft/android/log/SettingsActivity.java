package com.robotsoft.android.log;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.Toast;

public class SettingsActivity extends TabActivity {

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			initTab(TagActivity.class, "tags", R.string.tags,
					R.drawable.tab_tags);
			initTab(FilterActivity.class, "filter", R.string.filter,
					R.drawable.tab_filter);
		} catch (Throwable e) {
			Log.e("LogView", "Error with tab configuration", e);
		}
	}

	private void initTab(Class clz, String key, int txtId, int iconId) {
		try {
			Intent intent = new Intent(this, clz);
			TabHost.TabSpec spec = getTabHost()
					.newTabSpec(key)
					.setIndicator(getString(txtId),
							getResources().getDrawable(iconId))
					.setContent(intent);
			getTabHost().addTab(spec);
		} catch (Throwable e) {
			Log.e("LogView", "Error with tab configuration " + key, e);
		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.setting_option, menu);
		return true;
	}
	
	private void apply() {
		Activity  tab=getLocalActivityManager().getActivity("filter");
		if(tab!=null) {
			((FilterActivity)tab).popultate();
		}
		Intent i = new Intent(this, LogMain.class);
		startActivity(i);
		finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.apply:
			if (!Util.isActive(this)) {
				Toast.makeText(this, R.string.no_tag, Toast.LENGTH_LONG).show();
			} else {
				apply();
			}
			break;
		case R.id.exit:
			finish();
			break;
		}
		return true;
	}

	public void onBackPressed() {
		if (Util.isActive(this)) {
			apply();
		} else {
			super.onBackPressed();
		}
	}

}
