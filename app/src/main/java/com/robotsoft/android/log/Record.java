package com.robotsoft.android.log;

public class Record {
	private String time;
	private String pid;
	private String content;
	private int level;
	
	public Record(String time, String content, int level) {
		this(time, null, content, level);
	}

	public Record(String time, String pid, String content, int level) {
		this.time = time;
		this.pid = pid;
		this.content = content;
		this.level = level;
	}

	public String getTime() {
		return time;
	}

	public String getPid() {
		return pid;
	}

	public String getContent() {
		return content;
	}

	public int getLevel() {
		return level;
	}

}
