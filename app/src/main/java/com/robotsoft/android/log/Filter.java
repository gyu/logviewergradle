package com.robotsoft.android.log;

public class Filter {
	private int lineCount;
	private String pattern;
	private String time;
	
	public Filter(int lineCount, String time, String pattern) {
		this.lineCount = lineCount;
		this.pattern = pattern;
		this.time = time;
	}

	public int getLineCount() {
		return lineCount;
	}

	public String getPattern() {
		return pattern;
	}

	public String getTime() {
		return time;
	}
	

}
