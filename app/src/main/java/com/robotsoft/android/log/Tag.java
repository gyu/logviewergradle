package com.robotsoft.android.log;

public class Tag {
	private String name;
	private String value;
	private boolean active;
	private boolean selected;
	
	public Tag() {
		this.active = true;
		selected=false;
	}
	
	public Tag(String name, String value, boolean active) {
		super();
		this.name = name;
		this.value = value;
		this.active = active;
		selected=false;
	}
	public String getName() {
		return name;
	}
	public String getValue() {
		return value;
	}
	public boolean isActive() {
		return active;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

	
}
